package com.pideteq.pidencio.Fragments;

import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.telephony.PhoneNumberUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.pideteq.pidencio.Activities.MainActivity;
import com.pideteq.pidencio.Common.Common;
import com.pideteq.pidencio.R;

import java.net.URLEncoder;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {

    private ImageView profilePicture, restaurantService, promotionsService, marketService, partyService;
    private TextView userName;
    private FirebaseAuth firebaseAuth;
    private FirebaseUser user;
    private Button btnOrderFood;

    public HomeFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        firebaseAuth = FirebaseAuth.getInstance();
        user = firebaseAuth.getCurrentUser();
        restaurantService = view.findViewById(R.id.servicesRestaurantsImg);
        promotionsService = view.findViewById(R.id.servicesPromotionsImg);
        partyService = view.findViewById(R.id.servicesDrink);
        marketService = view.findViewById(R.id.servicesMarketImage);

        btnOrderFood = (Button)view.findViewById(R.id.btnOrderFood);
        userName = (TextView)view.findViewById(R.id.homeUserName);
        profilePicture = (ImageView)view.findViewById(R.id.homeProfileImg);

        if(user.getPhotoUrl() != null)
            Glide.with(this).load(user.getPhotoUrl()).thumbnail(0.5f).into(profilePicture);

        userName.setText(Common.user.getName());

        btnOrderFood.setOnClickListener(v -> {
            sendMessage();
        });

        return view;
    }

    public void sendMessage() {

        PackageManager packageManager = getActivity().getPackageManager();
        Intent intent = new Intent(Intent.ACTION_VIEW);
        String toNumber = "523741174969";
        String whatsAppMessage = "Pedido: ";

        try {
            String url = "https://api.whatsapp.com/send?phone="+ toNumber +"&text=" + URLEncoder.encode(whatsAppMessage, "UTF-8");
            intent.putExtra(Intent.EXTRA_TEXT, whatsAppMessage);
            intent.setType("text/plain");
            intent.setPackage("com.whatsapp");
            intent.setData(Uri.parse(url));

            startActivity(intent);

        }
        catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        restaurantService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Common.BUSINESS_TITLE = "Restaurantes";
                Common.BUSINESS_TURN = "restaurantes";
                Navigation.findNavController(view)
                        .navigate(R.id.action_homeFragment_to_restaurantFragment);
            }
        });


        promotionsService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Common.BUSINESS_TITLE = "Promociones";
                Common.BUSINESS_TURN = "promociones";
                Navigation.findNavController(view)
                        .navigate(R.id.action_homeFragment_to_restaurantFragment);
            }
        });

        partyService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Common.BUSINESS_TURN = "fiesta";
                Common.BUSINESS_TITLE = "Fiesta";
                Navigation.findNavController(view)
                        .navigate(R.id.action_homeFragment_to_restaurantFragment);
            }
        });

        marketService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Common.BUSINESS_TITLE = "Supermercados";
                Common.BUSINESS_TURN = "supermercado";
                Navigation.findNavController(view)
                        .navigate(R.id.action_homeFragment_to_restaurantFragment);
            }
        });
    }
}
