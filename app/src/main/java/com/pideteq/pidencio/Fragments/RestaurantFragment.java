package com.pideteq.pidencio.Fragments;


import android.content.Intent;
import android.os.Bundle;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.view.ViewCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.pideteq.pidencio.Adapters.RestaurantsAdapter;
import com.pideteq.pidencio.Common.Common;
import com.pideteq.pidencio.Helpers.WaitingDialog;
import com.pideteq.pidencio.R;
import com.pideteq.pidencio.ViewModels.RestaurantsViewModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class RestaurantFragment extends Fragment {

    @BindView(R.id.recyclerview_restaurants)
    RecyclerView recyclerView;
    @BindView(R.id.arrowBackRestaurants)
    ImageView arrowBack;
    @BindView(R.id.businessTurnLbl)
    TextView businessTitle;
    @BindView(R.id.proximamenteLbl)
    TextView info;


    private RestaurantsAdapter restaurantsAdapter;
    private NavController navController = null;
    private RestaurantsViewModel restaurantsViewModel;
    private WaitingDialog waitingDialog;

    Unbinder unbinder;

    public RestaurantFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        restaurantsViewModel = ViewModelProviders.of(this).get(RestaurantsViewModel.class);
        View view = inflater.inflate(R.layout.fragment_restaurant, container, false);
        unbinder = ButterKnife.bind(this, view);

        businessTitle.setText(Common.BUSINESS_TITLE + " en Tequila");


        init(view);

        restaurantsViewModel.getRestaurantsList().observe(this, restaurantModels -> {
            waitingDialog.dismissDialog();
            restaurantsAdapter = new RestaurantsAdapter(getContext(), restaurantModels, navController);
            if(restaurantModels.size() == 0){
                info.setVisibility(View.VISIBLE);
            }else{
                info.setVisibility(View.GONE);
                recyclerView.setAdapter(restaurantsAdapter);
                ViewCompat.setNestedScrollingEnabled(recyclerView, false);
            }

        });


        return view;
    }

    private void init(View view) {
        waitingDialog = new WaitingDialog(getActivity());
        waitingDialog.startWaiting();

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));

        arrowBack.setOnClickListener(v -> Navigation.findNavController(view).popBackStack());
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        navController = Navigation.findNavController(view);

        // This callback will only be called when MyFragment is at least Started.
        OnBackPressedCallback callback = new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                navController.popBackStack();
            }
        };
        requireActivity().getOnBackPressedDispatcher().addCallback(this, callback);
    }

}
