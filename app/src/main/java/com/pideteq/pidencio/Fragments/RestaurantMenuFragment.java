package com.pideteq.pidencio.Fragments;


import android.os.Bundle;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.view.ViewCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.pideteq.pidencio.Adapters.MenuAdapter;
import com.pideteq.pidencio.Common.Common;
import com.pideteq.pidencio.Helpers.WaitingDialog;
import com.pideteq.pidencio.R;
import com.pideteq.pidencio.ViewModels.MenuViewModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class RestaurantMenuFragment extends Fragment {


    @BindView(R.id.recyclerview_menu)
    RecyclerView recyclerView;
    @BindView(R.id.restaurantMenuLogo)
    ImageView imgRestaurantLogo;
    @BindView(R.id.menuRestaurantName)
    TextView restaurantName;
    @BindView(R.id.menuRestaurantCategory)
    TextView restaurantCategory;
    @BindView(R.id.menuRestaurantAddress)
    TextView restaurantAddress;
    @BindView(R.id.restaurantArrowBack)
    ImageView arrowBack;
    @BindView(R.id.proximamenteMenuLbl)
    TextView info;

    private MenuAdapter menuAdapter;
    private MenuViewModel menuViewModel;
    Unbinder unbinder;
    private NavController navController = null;
    private WaitingDialog waitingDialog;

    public RestaurantMenuFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        menuViewModel = ViewModelProviders.of(this).get(MenuViewModel.class);
        View view = inflater.inflate(R.layout.fragment_restaurant_menu, container, false);

        unbinder = ButterKnife.bind(this, view);


        init(view);

        menuViewModel.getMenuList().observe(this, foodList -> {
            waitingDialog.dismissDialog();
            menuAdapter = new MenuAdapter(getContext(), foodList, navController);
            if(foodList.size() == 0){
                info.setVisibility(View.VISIBLE);
            }else{
                info.setVisibility(View.GONE);
                recyclerView.setAdapter(menuAdapter);
                ViewCompat.setNestedScrollingEnabled(recyclerView, false);
            }

        });


        return view;
    }

    private void init(View view) {
        waitingDialog = new WaitingDialog(getActivity());
        waitingDialog.startWaiting();
        restaurantName.setText(Common.restaurant.getName());
        restaurantCategory.setText("Categoría: " + Common.restaurant.getCategory().replace("_", " "));
        restaurantAddress.setText("Dirección: " + Common.restaurant.getAddress());

        //Glide.with(this).load(Common.restaurant.getLogo()).thumbnail(0.5f).into(imgRestaurantLogo);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));

        arrowBack.setOnClickListener(v -> Navigation.findNavController(view).popBackStack());

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        navController = Navigation.findNavController(view);

        // This callback will only be called when MyFragment is at least Started.
        OnBackPressedCallback callback = new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                navController.popBackStack();
            }
        };
        requireActivity().getOnBackPressedDispatcher().addCallback(this, callback);
    }

}
