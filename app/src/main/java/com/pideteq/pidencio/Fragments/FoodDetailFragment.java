package com.pideteq.pidencio.Fragments;


import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.chip.Chip;
import com.google.android.material.chip.ChipGroup;
import com.google.gson.Gson;
import com.pideteq.pidencio.Common.Common;
import com.pideteq.pidencio.Database.CartDataBase;
import com.pideteq.pidencio.Database.CartDataSource;
import com.pideteq.pidencio.Database.CartItem;
import com.pideteq.pidencio.Database.LocalCartDataSource;
import com.pideteq.pidencio.EventBus.CounterCartEvent;
import com.pideteq.pidencio.Helpers.WaitingDialog;
import com.pideteq.pidencio.Model.AddonModel;
import com.pideteq.pidencio.Model.FoodModel;
import com.pideteq.pidencio.Model.SizeModel;
import com.pideteq.pidencio.R;
import com.pideteq.pidencio.ViewModels.FoodDetailViewModel;

import org.greenrobot.eventbus.EventBus;

import java.net.URLEncoder;
import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class FoodDetailFragment extends Fragment implements TextWatcher {

    private Unbinder unbinder;
    private FoodDetailViewModel foodDetailViewModel;
    private BottomSheetDialog addonBottomSheet;
    private CompositeDisposable compositeDisposable;
    private CartDataSource cartDataSource;

    private ChipGroup chip_group_addon;
    private EditText searchAddon;

    private WaitingDialog waitingDialog;

    @BindView(R.id.detailFoodName)
    TextView foodName;

    @BindView(R.id.detailFoodPrice)
    TextView foodPrice;

    @BindView(R.id.totalPriceLbl)
    TextView totalPriceLbl;

    @BindView(R.id.detailFoodImg)
    ImageView foodImg;

    @BindView(R.id.detailFoodSize)
    RadioGroup radioSize;

    @BindView(R.id.detailQuantity)
    ElegantNumberButton quantitySelected;

    @BindView(R.id.cardViewSizes)
    CardView cardViewSizes;

    @BindView(R.id.addonImage)
    ImageView addonImg;

    @BindView(R.id.detailBack)
    ImageView arrowBack;

    @BindView(R.id.selectedAddon)
    ChipGroup chip_group_user_selected_addon;


    @OnClick(R.id.addonImage)
    void onAddonClick(){
        if(Common.selectedFood.getAddon() != null){
            displayAddonList();
            addonBottomSheet.show();
        }
    }

    String getExtras(){
        StringBuilder sbuilder = new StringBuilder();
        if(Common.selectedFood.getUserSelectedAddon() != null){
            sbuilder.append(" Ingredientes/extras: \n");
            for(AddonModel addonModel : Common.selectedFood.getUserSelectedAddon()){
                sbuilder.append("- " + addonModel.getName() + "\n");
            }
        }
        return sbuilder.toString();
    }

    String getSize() {
        StringBuilder sbuilder = new StringBuilder();
        if(Common.selectedFood.getUserSelectedSize() != null){
            sbuilder.append(" Tamaño: \n");
            sbuilder.append("- " + Common.selectedFood.getUserSelectedSize().getName());
        }
        return sbuilder.toString();
    }

    @OnClick(R.id.bntAddFood)
    void onCartItemAdd(){
        Intent intent = new Intent(Intent.ACTION_VIEW);
        String toNumber = "523741174969";
        String whatsAppMessage = "Pedido a " + Common.restaurant.getName()  +": \n- " + quantitySelected.getNumber()
                + " " +Common.selectedFood.getName() + getExtras() + getSize();

        try {
            String url = "https://api.whatsapp.com/send?phone="+ toNumber +"&text=" + URLEncoder.encode(whatsAppMessage, "UTF-8");
            intent.putExtra(Intent.EXTRA_TEXT, whatsAppMessage);
            intent.setType("text/plain");
            intent.setPackage("com.whatsapp");
            intent.setData(Uri.parse(url));

            startActivity(intent);

        }
        catch (Exception e){
            e.printStackTrace();
        }
        /*CartItem cartItem = new CartItem();

        cartItem.setUid(Common.user.getUid());
        cartItem.setUserPhone(Common.user.getPhone());

        cartItem.setFoodId(Common.selectedFood.getId());
        cartItem.setFoodName(Common.selectedFood.getName());
        cartItem.setFoodPrice(Double.parseDouble(String.valueOf(Common.selectedFood.getPrice())));
        cartItem.setFoodImage(Common.restaurant.getLogo());
        cartItem.setFoodQuantity(Integer.valueOf(quantitySelected.getNumber()));
        cartItem.setFoodExtraPrice(Common.calculateExtraPrice(Common.selectedFood.getUserSelectedSize(), Common.selectedFood.getUserSelectedAddon()));

        if(Common.selectedFood.getUserSelectedAddon() != null)
            cartItem.setFoodAddOn(new Gson().toJson(Common.selectedFood.getUserSelectedAddon()));
        else
            cartItem.setFoodAddOn("Default");

        if(Common.selectedFood.getUserSelectedSize() != null)
            cartItem.setFoodSize(new Gson().toJson(Common.selectedFood.getUserSelectedSize()));
        else
            cartItem.setFoodSize("Default");




        cartDataSource.getItemWithAllOptionsInCart(Common.user.getUid(),
                cartItem.getFoodId(),
                cartItem.getFoodSize(),
                cartItem.getFoodAddOn())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<CartItem>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onSuccess(CartItem cartItemFromDB) {
                        if(cartItemFromDB.equals(cartItem)){
                            cartItemFromDB.setFoodExtraPrice(cartItem.getFoodPrice());
                            cartItemFromDB.setFoodAddOn(cartItem.getFoodAddOn());
                            cartItemFromDB.setFoodSize(cartItem.getFoodSize());
                            cartItemFromDB.setFoodQuantity(cartItemFromDB.getFoodQuantity() + cartItem.getFoodQuantity());

                            cartDataSource.updateCartItems(cartItemFromDB)
                                    .subscribeOn(Schedulers.io())
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribe(new SingleObserver<Integer>() {
                                        @Override
                                        public void onSubscribe(Disposable d) {

                                        }

                                        @Override
                                        public void onSuccess(Integer integer) {
                                            Toast.makeText(getContext(), "Carrito actualizado", Toast.LENGTH_SHORT).show();
                                            EventBus.getDefault().postSticky(new CounterCartEvent(true));
                                        }

                                        @Override
                                        public void onError(Throwable e) {
                                            Toast.makeText(getContext(), "[UPDATE CART]: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                                        }
                                    });
                        }else{
                            compositeDisposable.add(cartDataSource.insertOrReplaceAll(cartItem)
                                    .subscribeOn(Schedulers.io())
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribe(() -> {
                                        Toast.makeText(getContext(), "Agregado al carrito", Toast.LENGTH_SHORT).show();
                                        EventBus.getDefault().postSticky(new CounterCartEvent(true));
                                    }, throwable -> {
                                        Toast.makeText(getContext(), "[CART ERROR]: " + throwable.getMessage(), Toast.LENGTH_SHORT).show();
                                    }));
                        }

                    }

                    @Override
                    public void onError(Throwable e) {
                        if(e.getMessage().contains("empty")){
                            compositeDisposable.add(cartDataSource.insertOrReplaceAll(cartItem)
                                    .subscribeOn(Schedulers.io())
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribe(() -> {
                                        Toast.makeText(getContext(), "Agregado al carrito", Toast.LENGTH_SHORT).show();
                                        EventBus.getDefault().postSticky(new CounterCartEvent(true));
                                    }, throwable -> {
                                        Toast.makeText(getContext(), "[CART ERROR]: " + throwable.getMessage(), Toast.LENGTH_SHORT).show();
                                    }));
                        }else
                            Toast.makeText(getContext(), "[GET CART]: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });*/
    }

    private void displayAddonList() {
        if(Common.selectedFood.getAddon().size() > 0){
            chip_group_addon.clearCheck();
            chip_group_addon.removeAllViews();

            searchAddon.addTextChangedListener(this);

            for(AddonModel addonModel : Common.selectedFood.getAddon()){

                Chip chip = (Chip)getLayoutInflater().inflate(R.layout.layout_addon_item, null);
                chip.setText(new StringBuilder(addonModel.getName()).append("(+$")
                        .append(addonModel.getPrice()).append(")"));
                chip.setOnCheckedChangeListener((buttonView, isChecked) -> {
                    if(isChecked){
                        if(Common.selectedFood.getUserSelectedAddon() == null){
                            Common.selectedFood.setUserSelectedAddon(new ArrayList<>());
                        }
                        Common.selectedFood.getUserSelectedAddon().add(addonModel);
                    }
                });

                chip_group_addon.addView(chip);
            }

        }
    }

    public FoodDetailFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        foodDetailViewModel = ViewModelProviders.of(this).get(FoodDetailViewModel.class);

        View view = inflater.inflate(R.layout.fragment_food_detail, container, false);

        unbinder = ButterKnife.bind(this, view);

        initViews(view);

        foodDetailViewModel.getMutableLiveDataFood().observe(this, selectedFood -> {
            displayInfo(selectedFood);
        });

        return view;
    }

    private void initViews(View view) {
        waitingDialog = new WaitingDialog(getActivity());
        waitingDialog.startWaiting();
        compositeDisposable = new CompositeDisposable();
        cartDataSource = new LocalCartDataSource(CartDataBase.getInstance(getContext()).cartDAO());

        addonBottomSheet = new BottomSheetDialog(getContext(), R.style.DialogStyle);

        View layout_addon_display = getLayoutInflater().inflate(R.layout.layout_addon_display, null);

        chip_group_addon = (ChipGroup)layout_addon_display.findViewById(R.id.chip_group_addon);
        searchAddon = (EditText)layout_addon_display.findViewById(R.id.searchAddon);
        addonBottomSheet.setContentView(layout_addon_display);

        addonBottomSheet.setOnDismissListener(dialog -> {
            displayUserSelectedAddon();
            calculateTotalPrice();
        });

        quantitySelected.setOnClickListener(new ElegantNumberButton.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println("Presionado");
                calculateTotalPrice();
            }
        });

        arrowBack.setOnClickListener(v -> Navigation.findNavController(view).popBackStack());

        waitingDialog.dismissDialog();
    }

    private void displayUserSelectedAddon() {

        if(Common.selectedFood.getUserSelectedAddon() != null
            && Common.selectedFood.getUserSelectedAddon().size() > 0){
            chip_group_user_selected_addon.removeAllViews();
            for (AddonModel addonModel : Common.selectedFood.getUserSelectedAddon()){
                Chip chip = (Chip)getLayoutInflater().inflate(R.layout.layout_chip_with_delete_icon, null);
                chip.setText(new StringBuilder(addonModel.getName()).append("(+$")
                    .append(addonModel.getPrice()).append(")"));
                chip.setClickable(false);
                chip.setOnCloseIconClickListener(view -> {
                    //Remove when user selecte it
                    chip_group_user_selected_addon.removeView(view);
                    Common.selectedFood.getUserSelectedAddon().remove(addonModel);
                    calculateTotalPrice();
                });

                chip_group_user_selected_addon.addView(chip);
            }
        }else if(Common.selectedFood.getUserSelectedAddon() == null){
            chip_group_user_selected_addon.removeAllViews();
        }

    }

    private void displayInfo(FoodModel foodModel) {
        if(!Common.restaurant.getLogo().isEmpty())
            //Glide.with(getContext()).load(Common.restaurant.getLogo()).into(foodImg);
        foodName.setText(foodModel.getName());
        foodPrice.setText(Long.toString(foodModel.getPrice()) + "   Por pieza");
        if(Common.selectedFood.getSize().size() != 0){
            for(SizeModel sizeModel : Common.selectedFood.getSize()){
                RadioButton radioButton = new RadioButton(getContext());
                radioButton.setOnCheckedChangeListener((buttonView, isChecked) -> {
                    if(isChecked){
                        Common.selectedFood.setUserSelectedSize(sizeModel);
                    }

                    calculateTotalPrice();
                });

                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.MATCH_PARENT, 1.0f);

                radioButton.setLayoutParams(params);
                radioButton.setText(sizeModel.getName());
                radioButton.setTag(sizeModel.getPrice());

                radioSize.addView(radioButton);
            }

            if(radioSize.getChildCount() > 0){
                RadioButton radioButton = (RadioButton)radioSize.getChildAt(0);
                radioButton.setChecked(true);
            }

            calculateTotalPrice();
        }else{
            cardViewSizes.setVisibility(View.GONE);
        }
    }

    private void calculateTotalPrice() {
        System.out.println("entraaaaa");
        double totalPrice = Double.parseDouble(Common.selectedFood.getPrice().toString());
        double displayPrice=0.0;

        if(Common.selectedFood.getUserSelectedAddon() != null && Common.selectedFood.getUserSelectedAddon().size() > 0){
            for(AddonModel addonModel : Common.selectedFood.getUserSelectedAddon()){
                totalPrice += Double.parseDouble(addonModel.getPrice().toString());
            }
        }

        if(Common.selectedFood.getUserSelectedSize() != null){
            totalPrice += Double.parseDouble(Common.selectedFood.getUserSelectedSize().getPrice().toString());

        }


        displayPrice = totalPrice * (Integer.parseInt(quantitySelected.getNumber()));
        displayPrice = Math.round(displayPrice * 100.0/100.0);
        System.out.println("TotalPrice: " + displayPrice);

        totalPriceLbl.setText(new StringBuilder("$").append(Common.formatPrice(displayPrice)).toString());
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        //Nothing
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        chip_group_addon.clearCheck();
        chip_group_addon.removeAllViews();

        for(AddonModel addonModel : Common.selectedFood.getAddon()){
            if(addonModel.getName().toLowerCase().contains(s.toString().toLowerCase())){
                Chip chip = (Chip)getLayoutInflater().inflate(R.layout.layout_addon_item, null);
                chip.setText(new StringBuilder(addonModel.getName()).append("(+$")
                .append(addonModel.getPrice()).append(")"));
                chip.setOnCheckedChangeListener((buttonView, isChecked) -> {
                    if(isChecked){
                        if(Common.selectedFood.getUserSelectedAddon() == null){
                            Common.selectedFood.setUserSelectedAddon(new ArrayList<>());
                        }
                        Common.selectedFood.getUserSelectedAddon().add(addonModel);
                    }
                });

                chip_group_addon.addView(chip);
            }
        }
    }

    @Override
    public void afterTextChanged(Editable s) {
        //Nothing
    }

    @Override
    public void onStop() {
        compositeDisposable.clear();
        super.onStop();

    }
}
