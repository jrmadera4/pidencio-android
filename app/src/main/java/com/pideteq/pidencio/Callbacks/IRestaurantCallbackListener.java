package com.pideteq.pidencio.Callbacks;

import com.pideteq.pidencio.Model.RestaurantModel;

import java.util.List;

public interface IRestaurantCallbackListener {

    void onRestaurantsLoadSuccess(List<RestaurantModel> restaurantModelList);
    void onRestaurantsLoadFail(String message);

}
