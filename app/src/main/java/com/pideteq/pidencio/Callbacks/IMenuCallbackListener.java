package com.pideteq.pidencio.Callbacks;

import com.pideteq.pidencio.Model.FoodModel;

import java.util.List;

public interface IMenuCallbackListener {

    void onMenuLoadSuccess(List<FoodModel> foodModelList);
    void onMenuLoadFail(String message);
}
