package com.pideteq.pidencio.UI.Cart;

import android.content.Context;

import com.pideteq.pidencio.Common.Common;
import com.pideteq.pidencio.Database.CartDataBase;
import com.pideteq.pidencio.Database.CartDataSource;
import com.pideteq.pidencio.Database.CartItem;
import com.pideteq.pidencio.Database.LocalCartDataSource;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class CartViewModel extends ViewModel {

    private MutableLiveData<List<CartItem>> mutableLiveData;
    private CompositeDisposable compositeDisposable;
    private CartDataSource cartDataSource;

    public CartViewModel() {
        compositeDisposable = new CompositeDisposable();
    }

    public MutableLiveData<List<CartItem>> getMutableLiveDataCartItem() {
        if(mutableLiveData == null){
            mutableLiveData = new MutableLiveData<>();
        }

        getAllCartItems();
        return mutableLiveData;
    }

    public void initCartDataSource(Context context){
        cartDataSource = new LocalCartDataSource(CartDataBase.getInstance(context).cartDAO());
    }

    private void getAllCartItems() {
        compositeDisposable.add(cartDataSource.getAllCart(Common.user.getUid())
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(cartItems -> {
            mutableLiveData.setValue(cartItems);
        }, throwable -> {
            mutableLiveData.setValue(null);
        }));
    }

    public void onStop(){
        compositeDisposable.clear();
    }

}
