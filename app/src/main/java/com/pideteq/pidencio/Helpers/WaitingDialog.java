package com.pideteq.pidencio.Helpers;

import android.app.Activity;
import android.app.AlertDialog;
import android.view.LayoutInflater;

import com.pideteq.pidencio.R;

public class WaitingDialog {

    Activity activity;
    AlertDialog alertDialog;

    public WaitingDialog(Activity activity){
        this.activity = activity;
    }

    public void startWaiting(){
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        LayoutInflater inflater = activity.getLayoutInflater();

        builder.setView(inflater.inflate(R.layout.custom_dialog, null));

        builder.setCancelable(false);

        alertDialog = builder.create();
        alertDialog.show();
    }

    public void dismissDialog(){
        alertDialog.dismiss();
    }
}
