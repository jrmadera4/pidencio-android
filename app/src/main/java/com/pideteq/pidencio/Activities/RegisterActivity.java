package com.pideteq.pidencio.Activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.auth.User;
import com.pideteq.pidencio.Common.Common;
import com.pideteq.pidencio.Helpers.WaitingDialog;
import com.pideteq.pidencio.Model.UserModel;
import com.pideteq.pidencio.R;

import java.util.Arrays;
import java.util.Map;
import java.util.regex.Pattern;

public class RegisterActivity extends AppCompatActivity {

    private static final Pattern PASSWORD_PATTERN =
            Pattern.compile("^" +
                    "(?=.*[0-9])" +         //at least 1 digit
                    "(?=.*[a-z])" +         //at least 1 lower case letter
                    "(?=.*[A-Z])" +         //at least 1 upper case letter
                    "(?=\\S+$)" +           //no white spaces
                    ".{6,}" +               //at least 4 characters
                    "$");

    private FirebaseAuth firebaseAuth;
    private Button registerButton, customFb;
    private EditText emailInput, passwordInput;
    private CallbackManager callbackManager;
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private WaitingDialog waitingDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        waitingDialog = new WaitingDialog(this);

        firebaseAuth = FirebaseAuth.getInstance();

        customFb = findViewById(R.id.customFbButton);
        registerButton = findViewById(R.id.registerButton);
        emailInput = findViewById(R.id.registerEmailInput);
        passwordInput = findViewById(R.id.registerPasswordInput);

        callbackManager = CallbackManager.Factory.create();


        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validateEmail(emailInput.getText().toString()) &&
                        validatePassword(passwordInput.getText().toString()))
                    registerUser(emailInput.getText().toString(), passwordInput.getText().toString());
            }
        });

        customFb.setOnClickListener(v -> {
            waitingDialog.startWaiting();
            LoginManager.getInstance().logInWithReadPermissions(RegisterActivity.this,
                    Arrays.asList("email", "public_profile"));

            LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
                @Override
                public void onSuccess(LoginResult loginResult) {
                    handleFacebookAccessToken(loginResult.getAccessToken());
                }

                @Override
                public void onCancel() {
                    Toast.makeText(RegisterActivity.this, "Inicio cancelado por usuario",
                            Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onError(FacebookException exception) {
                    Log.e("FAIL", "App failed: " + exception);
                    Toast.makeText(RegisterActivity.this, "Autenticación fallida " + exception,
                            Toast.LENGTH_SHORT).show();
                }
            });
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Pass the activity result back to the Facebook SDK
        callbackManager.onActivityResult(requestCode, resultCode, data);

        super.onActivityResult(requestCode, resultCode, data);
    }

    private void registerUser(String email, String password){
        firebaseAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d("CreateUser", "createUserWithEmail:success");
                            FirebaseUser user = firebaseAuth.getCurrentUser();
                            Toast.makeText(RegisterActivity.this, "Inicio exitoso, espere...",
                                    Toast.LENGTH_SHORT).show();
                            checkUserFromFirebase(user.getUid());
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w("CreateUser", "createUserWithEmail:failure", task.getException());
                            Toast.makeText(RegisterActivity.this, "Autenticación fallida",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    private void handleFacebookAccessToken(AccessToken token) {
        Log.d("Facebook", "handleFacebookAccessToken:" + token);

        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        firebaseAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d("Facebook", "signInWithCredential:success");
                            FirebaseUser user = firebaseAuth.getCurrentUser();
                            Toast.makeText(RegisterActivity.this, "Inicio exitoso, espere...",
                                    Toast.LENGTH_SHORT).show();
                            checkUserFromFirebase(user.getUid());
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w("Facebook", "signInWithCredential:failure", task.getException());
                            Toast.makeText(RegisterActivity.this, "Autenticación fallida",
                                    Toast.LENGTH_SHORT).show();
                        }

                        // ...
                    }
                });
    }

    private void goToHomeActivity(Map userModel){
        Common.currentUser = userModel;
        startActivity(new Intent(RegisterActivity.this, MainMenu.class));
        waitingDialog.dismissDialog();
        finish();
    }

    private void checkUserFromFirebase(String uid) {

        db.collection("users").document(uid).get().addOnSuccessListener(documentSnapshot -> {
            if(documentSnapshot.exists()){
                Map <String, Object> userModel = documentSnapshot.getData();
                goToHomeActivity(userModel);
            }else{
                Intent intent = new Intent(RegisterActivity.this, RegisterUser.class);
                intent.putExtra("uid", uid);
                startActivity(intent);
                finish();
            }
        }).addOnFailureListener(e -> System.out.println("Error: " + e.getMessage()));
    }

    private Boolean validateEmail(String email) {

        if (email.isEmpty()) {
            Toast.makeText(getApplicationContext(), "Ingrese un correo",
                    Toast.LENGTH_SHORT).show();
            return false;
        } else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            Toast.makeText(getApplicationContext(), "Ingrese un correo valido",
                    Toast.LENGTH_SHORT).show();
            return false;
        }else {
            return true;
        }
    }

    private boolean validatePassword(String password){

        if (password.isEmpty()) {
            Toast.makeText(getApplicationContext(),"Ingrese una contraseña",
                    Toast.LENGTH_SHORT).show();
            return false;
        } else if (!PASSWORD_PATTERN.matcher(password).matches()) {
            Toast.makeText(getApplicationContext()
                    ,"Al menos una mayuscula, un numero y mas de 6 caracteres",
                    Toast.LENGTH_SHORT).show();
            return false;
        } else {
            return true;
        }
    }
}
