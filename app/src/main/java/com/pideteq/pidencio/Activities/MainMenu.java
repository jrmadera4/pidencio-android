package com.pideteq.pidencio.Activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.NavDestination;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Scheduler;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.andremion.counterfab.CounterFab;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.pideteq.pidencio.Common.Common;
import com.pideteq.pidencio.Database.CartDataBase;
import com.pideteq.pidencio.Database.CartDataSource;
import com.pideteq.pidencio.Database.LocalCartDataSource;
import com.pideteq.pidencio.EventBus.CounterCartEvent;
import com.pideteq.pidencio.EventBus.HideFABCart;
import com.pideteq.pidencio.Fragments.AccountFragment;
import com.pideteq.pidencio.Fragments.FavoritesFragment;
import com.pideteq.pidencio.Fragments.HomeFragment;
import com.pideteq.pidencio.Fragments.SearchFragment;
import com.pideteq.pidencio.Model.RestaurantModel;
import com.pideteq.pidencio.Model.UserModel;
import com.pideteq.pidencio.R;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;


public class MainMenu extends AppCompatActivity {

    private TextView userName;
    private FirebaseAuth firebaseAuth;
    private FirebaseUser user;
    private BottomNavigationView bottomNavigationView;
    private CartDataSource cartDataSource;

    @BindView(R.id.cartCounter)
    CounterFab counterFab;

    public final int MY_PERMISSION_REQUEST_INTERNET = 1;
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);

        ButterKnife.bind(this);
        cartDataSource = new LocalCartDataSource(CartDataBase.getInstance(this).cartDAO());

        firebaseAuth = FirebaseAuth.getInstance();
        user = firebaseAuth.getCurrentUser();

        bottomNavigationView = findViewById(R.id.bottomNavigationView);

        NavController navController = Navigation.findNavController(this, R.id.nav_host_controller);

        NavigationUI.setupWithNavController(bottomNavigationView, navController);


        //counterFab.setOnClickListener(v -> navController.navigate(R.id.action_homeFragment_to_cartFragment));

        checkInternetPermission();
        checkLocationPermission();

        countCartItem();
    }

    @Override
    protected void onResume() {
        super.onResume();
        countCartItem();
    }

    public boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                new AlertDialog.Builder(this)
                        .setTitle("Acceso a tu ubicación")
                        .setMessage("Autoriza este permiso para poder llevar tu comida hasta tu casa")
                        .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(MainMenu.this,
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                        MY_PERMISSIONS_REQUEST_LOCATION);
                            }
                        })
                        .create()
                        .show();


            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }


    private void checkInternetPermission(){
        if(ContextCompat.checkSelfPermission(MainMenu.this, Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED){
            if(ActivityCompat.shouldShowRequestPermissionRationale(MainMenu.this, Manifest.permission.INTERNET)){
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Permisos necesarios");
                builder.setMessage("Debes proporcionar acceso a Internet a la app para poder realizar tus pedidos");
                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ActivityCompat.requestPermissions(MainMenu.this,
                                new String[]{Manifest.permission.INTERNET}, MY_PERMISSION_REQUEST_INTERNET);

                    }
                });
                builder.setPositiveButton("Autorizar permiso", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ActivityCompat.requestPermissions(MainMenu.this,
                                new String[]{Manifest.permission.INTERNET}, MY_PERMISSION_REQUEST_INTERNET);
                    }
                });
                builder.show();
            }else {
                ActivityCompat.requestPermissions(MainMenu.this,
                        new String[]{Manifest.permission.INTERNET}, MY_PERMISSION_REQUEST_INTERNET);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                    }
                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

        }

    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
        checkUserStatus();
    }

    private void checkUserStatus(){
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        if (currentUser == null) {
            sendToLogin();
        }else{
            DocumentReference docRef = db.collection("users").document(currentUser.getUid());
            docRef.get().addOnCompleteListener(task -> {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        Common.user = document.toObject(UserModel.class);
                        System.out.println("UserName: " + Common.user.getName());
                        Log.d(Common.TAG_SUCCESS, "DocumentSnapshot data: " + document.getData());
                    } else {
                        Log.d(Common.TAG_FAIL, "No such document");
                    }
                } else {
                    Log.d(Common.TAG_FAIL, "get failed with ", task.getException());
                }
            });
        }
    }


    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    private void logOut() {
        firebaseAuth.signOut();
        sendToLogin();
    }

    private void sendToLogin() {
        Intent loginIntent = new Intent(MainMenu.this, MainActivity.class);
        startActivity(loginIntent);
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onCartCounter(CounterCartEvent event){
        if(event.isSuccess()){
            countCartItem();
        }
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onHideFABEvent(HideFABCart event){
        if(event.getHidden())
            counterFab.hide();
        else
            counterFab.show();
    }

    private void countCartItem() {
        cartDataSource.countItemInCart(Common.user.getUid())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<Integer>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onSuccess(Integer integer) {
                        System.out.println("CANTIDAD: " + integer);
                        counterFab.setCount(integer);
                    }

                    @Override
                    public void onError(Throwable e) {
                        //Toast.makeText(MainMenu.this, "Error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }
}
