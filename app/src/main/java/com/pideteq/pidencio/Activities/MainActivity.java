package com.pideteq.pidencio.Activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.pideteq.pidencio.Common.Common;
import com.pideteq.pidencio.Helpers.WaitingDialog;
import com.pideteq.pidencio.Model.UserModel;
import com.pideteq.pidencio.R;

import java.util.Arrays;
import java.util.Map;
import java.util.regex.Pattern;

public class MainActivity extends AppCompatActivity {

    private static final Pattern PASSWORD_PATTERN =
            Pattern.compile("^" +
                    "(?=.*[0-9])" +         //at least 1 digit
                    "(?=.*[a-z])" +         //at least 1 lower case letter
                    "(?=.*[A-Z])" +         //at least 1 upper case letter
                    "(?=\\S+$)" +           //no white spaces
                    ".{6,}" +               //at least 4 characters
                    "$");

    private EditText passwordInput, emailInput;
    private Button loginButton, customFb;
    private TextView registerButton;
    private FirebaseAuth firebaseAuth;
    private CallbackManager callbackManager;
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private WaitingDialog waitingDialog;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        checkGooglePlayServices();
        Log.i("ACTIVITY_CREATED", "login_activity - created");
        waitingDialog = new WaitingDialog(this);

        firebaseAuth = FirebaseAuth.getInstance();

        customFb = findViewById(R.id.customFbButtonLogin);
        passwordInput = findViewById(R.id.passwordInput);
        emailInput = findViewById(R.id.emailInput);
        loginButton = findViewById(R.id.loginButton);
        registerButton = findViewById(R.id.registerButton);

        callbackManager = CallbackManager.Factory.create();

        loginButton.setOnClickListener(v -> {
            waitingDialog.startWaiting();
            signIn(emailInput.getText().toString(), passwordInput.getText().toString());
        });

        customFb.setOnClickListener(v -> {
            waitingDialog.startWaiting();
            LoginManager.getInstance().logInWithReadPermissions(MainActivity.this,
                    Arrays.asList("email", "public_profile"));

            LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
                @Override
                public void onSuccess(LoginResult loginResult) {
                    handleFacebookAccessToken(loginResult.getAccessToken());
                }

                @Override
                public void onCancel() {
                    Toast.makeText(MainActivity.this, "Inicio cancelado",
                            Toast.LENGTH_SHORT).show();
                    waitingDialog.dismissDialog();
                }

                @Override
                public void onError(FacebookException exception) {
                    Log.e("FAIL", "App failed: " + exception);
                    Toast.makeText(MainActivity.this, "Autenticación fallida. " + exception,
                            Toast.LENGTH_SHORT).show();
                }
            });
        });

        registerButton.setOnClickListener(v -> startActivity(new Intent(MainActivity.this, RegisterActivity.class)));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);

        super.onActivityResult(requestCode, resultCode, data);
    }

    private void handleFacebookAccessToken(AccessToken token) {
        Log.d("Facebook", "handleFacebookAccessToken:" + token);

        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        firebaseAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, task -> {
                    if (task.isSuccessful()) {
                        Log.d("Facebook", "signInWithCredential:success");
                        FirebaseUser user = firebaseAuth.getCurrentUser();
                        Toast.makeText(MainActivity.this, "Inicio exitoso",
                                Toast.LENGTH_SHORT).show();
                        checkUserFromFirebase(user.getUid());

                    } else {
                        Log.w("Facebook", "signInWithCredential:failure", task.getException());
                        Toast.makeText(MainActivity.this, "Ocurrió un error",
                                Toast.LENGTH_SHORT).show();
                    }

                    // ...
                });
    }

    private void goToHomeActivity(Map userModel){
        Common.currentUser = userModel;
        startActivity(new Intent(MainActivity.this, MainMenu.class));
        waitingDialog.dismissDialog();
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void checkUserFromFirebase(String uid) {

        db.collection("users").document(uid).get().addOnSuccessListener(documentSnapshot -> {
            if(documentSnapshot.exists()){
                Map <String, Object> userModel = documentSnapshot.getData();
                goToHomeActivity(userModel);
            }else{
                waitingDialog.dismissDialog();
                Intent intent = new Intent(MainActivity.this, RegisterUser.class);
                intent.putExtra("uid", uid);
                startActivity(intent);
                finish();
            }
        }).addOnFailureListener(e -> System.out.println("Error: " + e.getMessage()));

    }

    private void signIn(String email, String password){
        firebaseAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, task -> {
                    if (task.isSuccessful()) {
                        Log.d("SignIn", "signInWithEmail:success");
                        FirebaseUser user = firebaseAuth.getCurrentUser();
                        Toast.makeText(getApplicationContext()
                                ,"Acceso exitoso",
                                Toast.LENGTH_SHORT).show();
                        checkUserFromFirebase(user.getUid());
                    } else {
                        waitingDialog.dismissDialog();
                        Log.w("SignIn", "signInWithEmail:failure", task.getException());
                        Toast.makeText(MainActivity.this, "Ocurrió un error",
                                Toast.LENGTH_SHORT).show();
                    }
                });
    }


    private void checkGooglePlayServices(){
        switch (GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(this)){
            case ConnectionResult.SERVICE_MISSING:
                GoogleApiAvailability.getInstance().getErrorDialog(this,ConnectionResult.SERVICE_MISSING,0).show();
                break;
            case ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED:
                GoogleApiAvailability.getInstance().getErrorDialog(this,ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED,0).show();
                break;
            case ConnectionResult.SERVICE_DISABLED:
                GoogleApiAvailability.getInstance().getErrorDialog(this,ConnectionResult.SERVICE_DISABLED,0).show();
                break;
        }
    }
}
