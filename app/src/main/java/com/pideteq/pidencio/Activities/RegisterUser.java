package com.pideteq.pidencio.Activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.firestore.FirebaseFirestore;
import com.pideteq.pidencio.Common.Common;
import com.pideteq.pidencio.Helpers.WaitingDialog;
import com.pideteq.pidencio.Model.UserModel;
import com.pideteq.pidencio.R;

public class RegisterUser extends AppCompatActivity {

    private EditText name, phone, address;
    private Button btnContinue;
    private String uid;
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private WaitingDialog waitingDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_user);
        waitingDialog = new WaitingDialog(this);

        Bundle bundle = getIntent().getExtras();
        uid = bundle.getString("uid");

        init();
    }

    private void init() {
        name = findViewById(R.id.fullNameRegister);
        phone = findViewById(R.id.phoneRegister);
        address = findViewById(R.id.addressRegister);
        btnContinue = findViewById(R.id.btnContinueRegister);

        btnContinue.setOnClickListener(v -> {
            waitingDialog.startWaiting();
            if(!name.getText().toString().isEmpty() && !address.getText().toString().isEmpty() &&
                    !phone.getText().toString().isEmpty()){
                insertUserData();
            }
        });
    }

    private void insertUserData() {
        UserModel userModel = new UserModel();
        userModel.setName(name.getText().toString());
        userModel.setPhone(phone.getText().toString());
        userModel.setAddress(address.getText().toString());
        userModel.setUid(uid);

        db.collection("users").document(uid).set(userModel)
            .addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    waitingDialog.dismissDialog();
                    startActivity(new Intent(RegisterUser.this, MainMenu.class));
                    finish();
                }
            }).addOnFailureListener(e -> {
                waitingDialog.dismissDialog();
                Toast.makeText(getApplicationContext(), "Ocurrió un error al crear la cuenta", Toast.LENGTH_SHORT).show();
            });
    }

}
