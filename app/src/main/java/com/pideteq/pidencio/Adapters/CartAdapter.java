package com.pideteq.pidencio.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextClock;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton;
import com.pideteq.pidencio.Common.Common;
import com.pideteq.pidencio.Database.CartItem;
import com.pideteq.pidencio.EventBus.UpdateItemInCart;
import com.pideteq.pidencio.R;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.MyViewHolder> {

    Context context;
    List<CartItem> cartItemList;

    public CartAdapter(Context context, List<CartItem> cartItemList) {
        this.context = context;
        this.cartItemList = cartItemList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.layout_cart_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        System.out.println("FOOOOD: " + cartItemList.get(position).getFoodImage());
        Glide.with(context).load(cartItemList.get(position).getFoodImage()).into(holder.imgProduct);
        holder.foodName.setText(new StringBuilder(cartItemList.get(position).getFoodName()));
        holder.foodPrice.setText(new StringBuilder("$ ")
                .append(cartItemList.get(position).getFoodPrice() + cartItemList.get(position).getFoodExtraPrice()));

        holder.productCounter.setNumber(String.valueOf(cartItemList.get(position).getFoodQuantity()));

        //Events

        holder.productCounter.setOnValueChangeListener((view, oldValue, newValue) -> {
            cartItemList.get(position).setFoodQuantity(newValue);

            EventBus.getDefault().postSticky(new UpdateItemInCart(cartItemList.get(position)));
        });
    }

    @Override
    public int getItemCount() {
        return cartItemList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{

        private Unbinder unbinder;

        @BindView(R.id.cartProductImage)
        ImageView imgProduct;

        @BindView(R.id.foodCountCart)
        ElegantNumberButton productCounter;

        @BindView(R.id.foodNameCart)
        TextView foodName;

        @BindView(R.id.foodPriceCart)
        TextView foodPrice;


        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            unbinder = ButterKnife.bind(this, itemView);
        }
    }
}
