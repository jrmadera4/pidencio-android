package com.pideteq.pidencio.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.pideteq.pidencio.Common.Common;
import com.pideteq.pidencio.Model.FoodModel;
import com.pideteq.pidencio.R;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.navigation.NavController;
import androidx.recyclerview.widget.RecyclerView;

public class MenuAdapter extends RecyclerView.Adapter<MenuAdapter.MyViewHolder> {

    private Context context;
    private List<FoodModel> menuList;
    private NavController navController = null;

    public MenuAdapter(Context context, List<FoodModel> menuList, NavController navController){
        this.context = context;
        this.menuList = menuList;
        this.navController = navController;
    }

    @NonNull
    @Override
    public MenuAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.menu_row, parent, false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MenuAdapter.MyViewHolder holder, int position) {
        holder.name.setText(menuList.get(position).getName());
        holder.description.setText(menuList.get(position).getDescription());
        holder.price.setText(Long.toString(menuList.get(position).getPrice()));

        holder.viewDetailsBtn.setOnClickListener(v -> {
            Common.selectedFood = menuList.get(position);
            navController.navigate(R.id.action_restaurantMenuFragment_to_foodDetailFragment);
        });
    }

    @Override
    public int getItemCount() {
        return menuList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{

        private TextView name, price, description;
        private Button viewDetailsBtn;
        private ImageView imgFood;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            viewDetailsBtn = itemView.findViewById(R.id.foodDetailsBtn);
            name = itemView.findViewById(R.id.foodNameRv);
            price = itemView.findViewById(R.id.foodPriceRv);
            description = itemView.findViewById(R.id.foodDescriptionRv);
            imgFood = itemView.findViewById(R.id.foodImageRv);
        }
    }
}
