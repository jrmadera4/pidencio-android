package com.pideteq.pidencio.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.pideteq.pidencio.Common.Common;
import com.pideteq.pidencio.Model.RestaurantModel;
import com.pideteq.pidencio.R;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.navigation.NavController;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class RestaurantsAdapter extends RecyclerView.Adapter<RestaurantsAdapter.MyViewHolder> {

    private Context context;
    private List <RestaurantModel> restaurantsList;
    private NavController navController = null;

    public RestaurantsAdapter(Context context, List<RestaurantModel> restaurantList, NavController navController){
        this.context = context;
        this.restaurantsList = restaurantList;
        this.navController = navController;
    }

    @NonNull
    @Override
    public RestaurantsAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.restaurant_row, parent, false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RestaurantsAdapter.MyViewHolder holder, int position) {
        holder.restaurantName.setText(restaurantsList.get(position).getName());
        holder.restaurantAddress.setText(restaurantsList.get(position).getAddress());
        holder.restaurantCategory.setText(restaurantsList.get(position).getCategory().replace("_", " "));

        if(!restaurantsList.get(position).getLogo().isEmpty())
            Glide.with(context).load(restaurantsList.get(position).getLogo()).thumbnail(0.5f).into(holder.restaurantImage);

        holder.menuBtn.setOnClickListener(v -> {
            navController.navigate(R.id.action_restaurantFragment_to_restaurantMenuFragment);
            Common.restaurant = restaurantsList.get(position);
        });
    }

    @Override
    public int getItemCount() {
        return restaurantsList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.restaurantAddressRv)
        TextView restaurantAddress;
        @BindView(R.id.restaurantNameRv)
        TextView restaurantName;
        @BindView(R.id.restaurantCategoryRv)
        TextView restaurantCategory;
        @BindView(R.id.restaurantMenuImgRv)
        ImageView restaurantImage;
        @BindView(R.id.restaurantMenuBtn)
        Button menuBtn;


        private ConstraintLayout constraintLayout;
        Unbinder unbinder;


        public MyViewHolder(View itemView) {
            super(itemView);
            unbinder = ButterKnife.bind(this, itemView);

            constraintLayout = itemView.findViewById(R.id.restaurantMainLayout);
        }
    }
}
