package com.pideteq.pidencio.Common;

import com.google.firebase.auth.FirebaseUser;
import com.pideteq.pidencio.Model.AddonModel;
import com.pideteq.pidencio.Model.FoodModel;
import com.pideteq.pidencio.Model.RestaurantModel;
import com.pideteq.pidencio.Model.SizeModel;
import com.pideteq.pidencio.Model.UserModel;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Map;

public class Common {
    public static final String USER_REFERENCES = "users";
    public static final String RESTAURANT_REFERENCES = "restaurants";
    public static Map<String, Object> currentUser;
    public static FirebaseUser firebaseUser;
    public static String BUSINESS_TURN = "";
    public static String BUSINESS_TITLE = "";

    public static final String TAG_SUCCESS = "success";
    public static final String TAG_FAIL = "fail";

    public static FoodModel selectedFood = new FoodModel();
    public static RestaurantModel restaurant = new RestaurantModel();
    public static UserModel user = new UserModel();

    public static String formatPrice(double price) {
        if(price != 0){
            DecimalFormat df = new DecimalFormat("#,##0.00");
            df.setRoundingMode(RoundingMode.UP);
            String finalPrice = new StringBuilder(df.format(price)).toString();
            return finalPrice;
        }else{
            return "0.00";
        }
    }

    public static Double calculateExtraPrice(SizeModel userSelectedSize, List<AddonModel> userSelectedAddon) {
        Double result = 0.0;
        if(userSelectedSize == null && userSelectedAddon == null)
            return 0.0;
        else if(userSelectedSize == null){
            for(AddonModel addonModel : userSelectedAddon)
                result += addonModel.getPrice();
            return result;
        }else if(userSelectedAddon == null){
            return userSelectedSize.getPrice()*1.0;
        }else{
            result = userSelectedSize.getPrice()*1.0;
            for(AddonModel addonModel : userSelectedAddon)
                result += addonModel.getPrice();
            return result;

        }
    }
}
