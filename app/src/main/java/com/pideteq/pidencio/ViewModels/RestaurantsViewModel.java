package com.pideteq.pidencio.ViewModels;

import android.util.Log;

import com.facebook.internal.Mutable;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.pideteq.pidencio.Adapters.RestaurantsAdapter;
import com.pideteq.pidencio.Callbacks.IRestaurantCallbackListener;
import com.pideteq.pidencio.Common.Common;
import com.pideteq.pidencio.Helpers.WaitingDialog;
import com.pideteq.pidencio.Model.RestaurantModel;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import androidx.recyclerview.widget.LinearLayoutManager;

public class RestaurantsViewModel extends ViewModel implements IRestaurantCallbackListener{

    private MutableLiveData<List<RestaurantModel>> restaurantsList;
    private MutableLiveData<String> messageError;
    private IRestaurantCallbackListener restaurantCallbackListener;

    public MutableLiveData<List<RestaurantModel>> getRestaurantsList() {
        if(restaurantsList == null){
            restaurantsList = new MutableLiveData<>();
            messageError = new MutableLiveData<>();
            loadRestaurants();
        }
        return restaurantsList;
    }

    private void loadRestaurants() {
        List<RestaurantModel> tempList = new ArrayList<>();
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection(Common.RESTAURANT_REFERENCES)
                .whereEqualTo("businessTurn", Common.BUSINESS_TURN)
                .get().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                for (QueryDocumentSnapshot document : task.getResult()) {
                    RestaurantModel restaurantModel = document.toObject(RestaurantModel.class);
                    restaurantModel.setId(document.getId());
                    tempList.add(restaurantModel);
                }

                restaurantCallbackListener.onRestaurantsLoadSuccess(tempList);
            } else {
                Log.w(Common.TAG_FAIL, "Error getting documents.", task.getException());
            }

        }).addOnFailureListener(e -> {

        });
    }

    public void setRestaurantsList(MutableLiveData<List<RestaurantModel>> restaurantsList) {
        this.restaurantsList = restaurantsList;
    }

    public MutableLiveData<String> getMessageError() {
        return messageError;
    }

    public void setMessageError(MutableLiveData<String> messageError) {
        this.messageError = messageError;
    }

    public RestaurantsViewModel(){
        restaurantCallbackListener = this;

    }

    @Override
    public void onRestaurantsLoadSuccess(List<RestaurantModel> restaurantModelList) {
        restaurantsList.setValue(restaurantModelList);
    }

    @Override
    public void onRestaurantsLoadFail(String message) {
        messageError.setValue(message);
    }
}
