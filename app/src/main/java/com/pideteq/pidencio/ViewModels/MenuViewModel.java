package com.pideteq.pidencio.ViewModels;

import android.util.Log;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.pideteq.pidencio.Callbacks.IMenuCallbackListener;
import com.pideteq.pidencio.Common.Common;
import com.pideteq.pidencio.Model.FoodModel;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class MenuViewModel extends ViewModel implements IMenuCallbackListener {

    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private CollectionReference menuRef = db.collection("menu");

    private MutableLiveData<List<FoodModel>> menuList;
    private MutableLiveData<String> messageError;
    private IMenuCallbackListener menuCallbackListener;

    public MutableLiveData<List<FoodModel>> getMenuList() {
        if(menuList == null){
            menuList = new MutableLiveData<>();
            messageError = new MutableLiveData<>();
            loadMenu();
        }
        return menuList;
    }

    private void loadMenu() {
        List<FoodModel> temp = new ArrayList<>();
        menuRef.document(Common.restaurant.getMenuId()).collection("food").get()
                .addOnSuccessListener(queryDocumentSnapshots -> {
                    for (QueryDocumentSnapshot document : queryDocumentSnapshots) {
                        FoodModel foodModel = document.toObject(FoodModel.class);
                        foodModel.setId(document.getId());
                        foodModel.setImage(Common.restaurant.getLogo());
                        temp.add(foodModel);
                    }
                    menuCallbackListener.onMenuLoadSuccess(temp);
                }).addOnFailureListener(e -> Log.d(Common.TAG_FAIL, "Error getting documents: " + e.getMessage()));

    }

    public MenuViewModel(){
        menuCallbackListener = this;
    }

    public void setMenuList(MutableLiveData<List<FoodModel>> menuList) {
        this.menuList = menuList;
    }

    public MutableLiveData<String> getMessageError() {
        return messageError;
    }

    public void setMessageError(MutableLiveData<String> messageError) {
        this.messageError = messageError;
    }

    @Override
    public void onMenuLoadSuccess(List<FoodModel> foodModelList) {
        menuList.setValue(foodModelList);
    }

    @Override
    public void onMenuLoadFail(String message) {
        messageError.setValue(message);
    }
}
