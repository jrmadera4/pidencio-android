package com.pideteq.pidencio.ViewModels;

import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.pideteq.pidencio.Common.Common;
import com.pideteq.pidencio.Model.AddonModel;
import com.pideteq.pidencio.Model.FoodModel;
import com.pideteq.pidencio.Model.SizeModel;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class FoodDetailViewModel extends ViewModel {

    private MutableLiveData<FoodModel> foodMenuList;
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private CollectionReference menuRef = db.collection("menu");

    public MutableLiveData<FoodModel> getMutableLiveDataFood() {
        if(foodMenuList == null){
            foodMenuList = new MutableLiveData<>();
            loadFoodSize();
        }

        return foodMenuList;
    }

    private void loadFoodSize() {
        ArrayList<SizeModel> temp = new ArrayList<>();
        System.out.println("Log: Size: " + Common.selectedFood.getId());
        menuRef.document(Common.restaurant.getMenuId()).collection("food").
                document(Common.selectedFood.getId()).collection("size").get()
                .addOnSuccessListener(queryDocumentSnapshots -> {
                    for (QueryDocumentSnapshot document : queryDocumentSnapshots) {
                        SizeModel sizeModel = document.toObject(SizeModel.class);
                        System.out.println("Bonito " + sizeModel.getName() + ", " + sizeModel.getPrice());
                        temp.add(sizeModel);
                    }
                    Common.selectedFood.setSize(temp);
                    loadAddon();
                }).addOnFailureListener(e -> Log.e(Common.TAG_FAIL, "[FAIL_FIRESTORE] - " + e));

    }

    private void loadAddon(){
        ArrayList<AddonModel> temp = new ArrayList<>();
        menuRef.document(Common.restaurant.getMenuId()).collection("food").
                document(Common.selectedFood.getId()).collection("addon").get()
                .addOnSuccessListener(queryDocumentSnapshots -> {
                    for (QueryDocumentSnapshot document : queryDocumentSnapshots) {
                        AddonModel addonModel = document.toObject(AddonModel.class);
                        temp.add(addonModel);
                    }
                    Common.selectedFood.setAddon(temp);
                    foodMenuList.setValue(Common.selectedFood);
                }).addOnFailureListener(e -> Log.e(Common.TAG_FAIL, "[FAIL_FIRESTORE] - " + e));
    }
}
