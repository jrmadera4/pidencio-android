package com.pideteq.pidencio.Model;

import java.util.Date;

public class RestaurantModel {

    private String name;
    private String address;
    private String phone;
    private String avgRaiting;
    private String category;
    private String logo;
    private String id;
    private String menuId;
    private String business_turn;
    private Date endTime;
    private Date startTime;

    public String getBusiness_turn() {
        return business_turn;
    }

    public void setBusiness_turn(String business_turn) {
        this.business_turn = business_turn;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    private String time;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    private String city;

    public RestaurantModel() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAvgRaiting() {
        return avgRaiting;
    }

    public void setAvgRaiting(String avgRaiting) {
        this.avgRaiting = avgRaiting;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getMenuId() {
        return menuId;
    }

    public void setMenuId(String menuId) {
        this.menuId = menuId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}
