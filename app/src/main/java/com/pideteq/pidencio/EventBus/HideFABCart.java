package com.pideteq.pidencio.EventBus;

public class HideFABCart {

    private Boolean hidden;

    public HideFABCart(Boolean hidden) {
        this.hidden = hidden;
    }

    public Boolean getHidden() {
        return hidden;
    }

    public void setHidden(Boolean hidden) {
        this.hidden = hidden;
    }
}
